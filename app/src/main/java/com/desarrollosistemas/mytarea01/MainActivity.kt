package com.desarrollosistemas.mytarea01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnProcesar.setOnClickListener {

           val miEdad = edtEdad.text.toString().toInt()

           val  edadCalculado = if ( miEdad >= 18) {
                "mayor"
            } else {
                "menor"
            }

            tvResultado.text = "Usted es $edadCalculado de edad"
        }
    }
}

